# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-mkinitfs
pkgver=1.4.1
pkgrel=5
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	boot-deploy
	busybox-extras
	btrfs-progs
	bzip2
	cryptsetup
	device-mapper
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	lz4
	multipath-tools
	parted
	postmarketos-fde-unlocker
	unudhcpd
	xz
	"
makedepends="go"
replaces="mkinitfs"
triggers="$pkgname.trigger=/etc/postmarketos-mkinitfs/hooks:/usr/share/kernel/*:/usr/share/postmarketos-mkinitfs-triggers"
source="
	https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/archive/$pkgver/postmarketos-mkinitfs-$pkgver.tar.gz
	00-default.modules
	init.sh
	init_functions.sh
	"
install="$pkgname.post-upgrade"
arch="all"
license="GPL-2.0-or-later"
provides="mkinitfs=3.6.0"

export GOPATH="$srcdir"
export CGO_ENABLED=0

build() {
	# "-s -w" build a stripped binary
	go build -v -ldflags="-s -w"
}

package() {
	install -Dm644 "$srcdir/init_functions.sh" \
		"$pkgdir/usr/share/postmarketos-mkinitfs/init_functions.sh"

	install -Dm755 "$srcdir/init.sh" \
		"$pkgdir/usr/share/postmarketos-mkinitfs/init.sh"

	install -Dm644 "$srcdir/00-default.modules" \
		"$pkgdir/etc/postmarketos-mkinitfs/modules/00-default.modules"

	install -Dm755 postmarketos-mkinitfs \
		"$pkgdir/sbin/postmarketos-mkinitfs"

	ln -s /sbin/postmarketos-mkinitfs \
		"$pkgdir/sbin/mkinitfs"

	mkdir -p "$pkgdir/etc/postmarketos-mkinitfs/hooks/"
}

check() {
	go test ./...
}

sha512sums="
f827acb0a10c0e5c1631f1b712e91fde7eb4ade0f1174eb2ef1754018bf4518ea1ad3229fd335c25fb0c6fe46ae20890f5cf43e58c8143ae17b5ab9bb36f0199  postmarketos-mkinitfs-1.4.1.tar.gz
1297ca5349326406a9db64a1dac47a822c257d781d1ecc6215b5aca7e2d22fa95396448318d6163dc8b172e2ce1d32ee846d32d796234fa4507016d39d3c4a82  00-default.modules
ccdceaa710d97d6f57d8d66bfcbec448486d08083341712303f62123039f729229b88528308e411a308a2b90b81b60de89fe91143a6facbb11cbc9b4055eeaec  init.sh
b6f433aa5a758e58ae4745f2b11f691f4d3ffbef35d9002bc2721ba797b3e7653e9449b6a1b503c47d725f644e0d7c0e5426f8a575f4cc437c21cfc6e2f4cdf3  init_functions.sh
"
